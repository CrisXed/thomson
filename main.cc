#include "G4RunManager.hh"
#include "G4UImanager.hh"

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#include "ThomsonDetectorConstruction.hh"
#include "ThomsonPhysicsList.hh"
#include "ThomsonPrimaryGeneratorAction.hh"
#include "ThomsonEventAction.hh"
#include "ThomsonRunAction.hh"
#include "ThomsonDetectorMessenger.hh"
#include "ThomsonActionInitialization.hh"
#include "ThomsonGlobals.hh"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

void printHelp(std::string exe);

int main(int argc, char** argv)
{
  // Init stuff here
  bool verbose = true;
  int processedArguments = 0;
  double bulbDiameter = 13;
  double neckDiameter = 3.8;
  double neckLength = 13;
  double screenHeight = 5.4;
  double screenWidth = 9;
  double screenAngle = 15;
  bool realisticFields = false;
  double scintillationYield = 1;
  bool noGlass = false;

  for(int i = 0; i < argc-1; ++i)
  {
    std::string arg = argv[1+i];
    if(arg.find("--help") != std::string::npos)
    {
      printHelp(argv[0]);
      return 0;
    }

    std::stringstream temp;
    if(arg.find("--bulbDiameter") != std::string::npos)
    {
      temp << argv[2+i];
      temp >> bulbDiameter;
      if(bulbDiameter < 5) bulbDiameter = 5;
      processedArguments += 2;
      ++i;
      continue;
    }
    if(arg.find("--neckDiameter") != std::string::npos)
    {
      temp << argv[2+i];
      temp >> neckDiameter;
      if(neckDiameter < 1) neckDiameter = 1;
      processedArguments += 2;
      ++i;
      continue;
    }
    if(arg.find("--neckLength") != std::string::npos)
    {
      temp << argv[2+i];
      temp >> neckLength;
      if(neckLength < 5) neckLength = 5;
      processedArguments += 2;
      ++i;
      continue;
    }
    if(arg.find("--screenHeight") != std::string::npos)
    {
      temp << argv[2+i];
      temp >> screenHeight;
      if(screenHeight < 1) screenHeight = 1;
      processedArguments += 2;
      ++i;
      continue;
    }
    if(arg.find("--screenWidth") != std::string::npos)
    {
      temp << argv[2+i];
      temp >> screenWidth;
      if(screenWidth < 1) screenWidth = 1;
      processedArguments += 2;
      ++i;
      continue;
    }
    if(arg.find("--screenAngle") != std::string::npos)
    {
      temp << argv[2+i];
      temp >> screenAngle;
      processedArguments += 2;
      ++i;
      continue;
    }
    if(arg.find("--realisticFields") != std::string::npos)
    {
      realisticFields = true;
      ++processedArguments;
      continue;
    }
    if(arg.find("--scintillationYield") != std::string::npos)
    {
      temp << argv[2+i];
      temp >> scintillationYield;
      if(scintillationYield <= 0)
        scintillationYield = 1;
      processedArguments += 2;
      ++i;
      continue;
    }
    if(arg.find("--onlyRootOutput") != std::string::npos)
    {
      OnlyRootOutput = true;
      ++processedArguments;
      continue;
    }
    if(arg.find("--noGlass") != std::string::npos)
    {
      noGlass = true;
      ++processedArguments;
      continue;
    }

    break;
  }

  bulbDiameter *= CLHEP::cm;
  neckDiameter *= CLHEP::cm;
  neckLength   *= CLHEP::cm;
  screenHeight *= CLHEP::cm;
  screenWidth  *= CLHEP::cm;
  screenAngle  *= CLHEP::deg;

  // construct the default run manager
  G4RunManager* runManager = new G4RunManager;

  // set mandatory initialization classes
  if(verbose)
    G4cout << "Setting mandatory Initialization classes" << std::endl;
  ThomsonDetectorConstruction* ThomsonConstruction = new ThomsonDetectorConstruction(bulbDiameter, neckDiameter, neckLength, screenHeight, screenWidth, screenAngle, realisticFields, noGlass);
  runManager->SetUserInitialization(ThomsonConstruction);
  ThomsonDetectorMessenger* ThomsonMessenger = new ThomsonDetectorMessenger(ThomsonConstruction);
  runManager->SetUserInitialization(new ThomsonPhysicsList(scintillationYield));
  ThomsonActionInitialization* ThomsonInitializer = new ThomsonActionInitialization;
  runManager->SetUserInitialization(ThomsonInitializer);

  // set mandatory user action class (This has been moved into the ThomsonActionInitialization class)
//  runManager->SetUserAction(new ThomsonPrimaryGeneratorAction);
//  runManager->SetUserAction(new ThomsonRunAction);
//  runManager->SetUserAction(new ThomsonEventAction);

  // Initialize G4 kernel
  if(verbose)
    G4cout << "Initializing the G4 kernel" << std::endl;
  runManager->Initialize();

  #ifdef G4VIS_USE
  // Visualization manager
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
  #endif

  // Get the pointer to the User Interface manager
  G4UImanager* UImanager = G4UImanager::GetUIpointer();

  G4UIExecutive* ui = NULL;

  if (argc == 1 + processedArguments)
  {
    // interactive mode : define UI session
    #ifdef G4UI_USE
    ui = new G4UIExecutive(argc, &argv[processedArguments]);
    #endif
  }

  //G4cout << *(G4Isotope::GetIsotopeTable()) << std::endl;
  //G4cout << *(G4Element::GetElementTable()) << std::endl;
  //G4cout << *(G4Material::GetMaterialTable()) << std::endl;

  if(ui)
  {
    ui->SessionStart();
    delete ui;
  }
  else
  {
    if(argc != 1 + processedArguments)
    {
      // batch mode
      G4String command = "/control/execute ";
      G4String fileName = argv[1+processedArguments];
      UImanager->ApplyCommand(command+fileName);
    }
  }

  // job termination
  #ifdef G4VIS_USE
  delete visManager;
  #endif
  delete runManager;
  return 0;
}

void printHelp(std::string exe)
{
  std::cout << "This program runs a simulation of the Thomson experiment (http://en.wikipedia.org/wiki/J._J._Thomson#Experiment_to_measure_the_mass_to_charge_ratio_of_cathode_rays)" << std::endl << std::endl;

  std::cout << "You can use the following options:" << std::endl;
  std::cout << "  bulbDiameter       - The diameter of the bulb portion of the tube, given in cm, minimum 5cm" << std::endl;
  std::cout << "  neckDiameter       - The diameter of the neck portion of the tube, given in cm, minimum 1cm (this value might be adjusted to account for the width of the electron beam)" << std::endl;
  std::cout << "  neckLength         - The length of the neck portion of the tube, given in cm, minimum 5cm" << std::endl;
  std::cout << "  screenHeight       - The height of the screen, given in cm, minimum 1cm" << std::endl;
  std::cout << "  screenWidth        - The width of the screen, given in cm, minimum 1cm" << std::endl;
  std::cout << "  screenAngle        - The angle with which the screen is placed with respect to the beam axis, given in degrees" << std::endl;
  std::cout << "  realisticFields    - This option toggles realistic fields or not, when this option is chosen, instead of having a uniform EM field only in the target area, the EM field will be present within the full vacuum volume of the tube and the fields will be computed for each point" << std::endl;
  std::cout << "  scintillationYield - This option controls the photon yield of the scintillation process, it can not be negative" << std::endl;
  std::cout << "  onlyRootOutput     - This option toggles the output of the dat files with the information about the hits on the sensitive detectors" << std::endl;
  std::cout << "  noGlass            - This option toggles if the tube should be made of glass or not, when the tube is not made of glass, it is made of air, efectively turning off any reflections on the tube" << std::endl;
  std::cout << "(Depending on the values given to the screen, the screen could be defined in such a way that part of it is outside the tube, in these cases the dimensions of the screen will be scaled down until it fits, this scaling will maintain the relative proportions of the dimensions)" << std::endl;

  std::cout << std::endl;
  std::cout << "Example: '" << exe << " --bulbDiameter 13 --neckDiameter 3.8 --neckLength 13 --screenHeight 5.4 --screenWidth 9 --screenAngle 15 --scintillationYield 1'" << std::endl;
  std::cout << "(The default values are those given in the example)" << std::endl;
  return;
}
