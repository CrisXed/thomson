#include <TROOT.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TVectorD.h>
#include <TSystem.h>
#include <TCanvas.h>
#include <TH2F.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TMath.h>

#include <RooRealVar.h>
#include <RooDataSet.h>
#include <RooGaussian.h>
#include <RooPlot.h>
#include <RooFitResult.h>
#include <RooAddPdf.h>

#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>

#include <vector>
#include <algorithm>

#define MAX_RUNS 67
#define DO_HISTO false
#define VERBOSE false

std::string itoa_len(int number, int len)
{
  std::stringstream ss; //create a stringstream
  ss << std::setw(len) << std::setfill('0') << number;    //add number to the stream
  return ss.str(); //return a string with the contents of the stream
}

std::string itoa(int number)
{
  std::stringstream ss; //create a stringstream
  ss << number;    //add number to the stream
  return ss.str(); //return a string with the contents of the stream
}

std::string ftoa_fixed_precision(double number, int decimals)
{
  std::stringstream ss; //create a stringstream
  ss << std::fixed << std::setprecision(decimals) << number;    //add number to the stream
  return ss.str(); //return a string with the contents of the stream
}

void Analyse()
{
  gSystem->mkdir("Screen");
  gSystem->mkdir("Plots");
  gSystem->mkdir("Sensel");

  TFile file("Output.root", "UPDATE");
  TCanvas c1("c1", "c1", 800, 600);

  std::stringstream results;

  for(int i = 0; i <= MAX_RUNS; ++i)
  {
    file.cd();
    std::string dirname = "Run_";
    dirname += itoa_len(i, 3);

    TDirectory* dir = file.GetDirectory(dirname.c_str());
    if(dir == NULL)
      continue;
    dir->cd();

    TTree* Hits;
    dir->GetObject("SelectedHits", Hits);
    if(Hits == NULL)
    {
      TTree* AllHits;
      dir->GetObject("Hits", AllHits);
      Hits = AllHits->CopyTree("LocalZ<-0.00025");
      Hits->SetName("SelectedHits");
      Hits->Write();
    }

    std::string title;
    double beamE, plateV;
    double coilA;
    double screenWidth, screenHeight, screenAngle;
    bool realisticFields;

    TVectorD* StatusVec;
    dir->GetObject("StatusVec", StatusVec);
    beamE = (double)(*StatusVec)[0];  // keV
    plateV = (double)(*StatusVec)[1]; // kV
    coilA = (double)(*StatusVec)[2];  // A
    screenWidth = (double)(*StatusVec)[3]; // cm
    screenHeight = (double)(*StatusVec)[4]; // cm
    screenAngle = (double)(*StatusVec)[5]; // deg
    if((double)(*StatusVec)[6] < 0)
      realisticFields = false;
    else
      realisticFields = true;

    std::cout << "Run " << i << ":" << std::endl;
    std::cout << "  Beam Energy:   " << ftoa_fixed_precision(beamE, 2) << " keV" << std::endl;
    std::cout << "  Plate Voltage: " << ftoa_fixed_precision(plateV, 2) << " kV" << std::endl;
    std::cout << "  Coil Current:  " << ftoa_fixed_precision(coilA, 2) << " A" << std::endl;

    title = ftoa_fixed_precision(beamE, 2)+"keV:"+ftoa_fixed_precision(plateV, 2)+"kV:"+ftoa_fixed_precision(coilA, 2)+"A";
    std::cout << "  " << title << std::endl;

    TH2F hist((dirname+"_hist").c_str(), (title+";x(cm);y(cm)").c_str(), 100, -screenWidth/2, screenWidth/2, 100, -screenHeight/2, screenHeight/2);
    TGraph* graph = NULL;

    if(DO_HISTO)
    {
      Hits->Draw(("LocalY:LocalX>>"+dirname+"_hist").c_str());
      hist.Draw();
    }
    else
    {
      gStyle->SetOptStat(0);
      Hits->SetEstimate(Hits->GetEntries());
      Hits->Draw("LocalY:LocalX", "", "goff");
      //std::cout << "Entries in graph: " << Hits->GetEntries() << std::endl;
      graph = new TGraph(Hits->GetEntries(), Hits->GetV2(), Hits->GetV1());

      hist.Draw();
      graph->Draw("P");
    }

    c1.SaveAs(("Screen/"+dirname+".png").c_str());
    c1.SaveAs(("Screen/"+dirname+".pdf").c_str());
    if(graph != NULL)
    {
      //std::cout << "Deleting graph" << std::endl;
      delete graph;
      graph = NULL;
    }

    if(plateV == 0.0)
    {
      // Compute Radius (The result will only make sense for those runs without E field)
      Hits->SetEstimate(Hits->GetEntries());
      std::vector<double> xVals, yVals, Radius;
      Hits->Draw("LocalY:LocalX", "", "goff");
      xVals.assign(Hits->GetV2(), Hits->GetV2()+Hits->GetEntries());
      yVals.assign(Hits->GetV1(), Hits->GetV1()+Hits->GetEntries());
      for(int j = 0; j < Hits->GetEntries(); ++j)
      {
        if(yVals[j] != 0.0)
        {
          double x = (xVals[j]+screenWidth/2)/100; //Convert from cm to m in the right frame
          double y = (yVals[j])/100;

          double R = (y*y + x*x)/abs(2*y);
          Radius.push_back(R*100); // Save radius in cm
        }
      }

      std::sort(Radius.begin(), Radius.end());
      double min = 0.0;
      double max = Radius[Radius.size()-1];
      double realMax = max;
      double median = Radius[Radius.size()/2];
      double asym = (median)/(max-median);
      if(asym < 1)
        asym = 1/asym;
      if( asym > 2.5)
      {
        std::cout << "Correcting Range: " << min << "-" << max << " to ";
        double temp = median;
        if(max-median < temp)
          temp = max-median;

        min = median - 2.5*temp;
        max = median + 2.5*temp;
        if(min < 0)
        {
          max = max - min;
          min = 0;
        }
        std::cout << min << "-" << max << std::endl;
      }
      gStyle->SetOptStat(111111);
      std::cout << "Min: " << min << "; Med: " << median << "; Max: " << max << "; Asym: " << asym << std::endl;
      TH1F Radius_hist("Radius", (title+";R(cm);N").c_str(), 50, min, max);
      for(unsigned int j = 0; j < Radius.size(); ++j)
      {
        Radius_hist.Fill(Radius[j]);
      }
      Radius_hist.Draw();
      c1.SaveAs(("Plots/"+dirname+"_Radius.png").c_str());
      c1.SaveAs(("Plots/"+dirname+"_Radius.pdf").c_str());

      int binmax = Radius_hist.GetMaximumBin();
      double maxX = Radius_hist.GetXaxis()->GetBinCenter(binmax);

      TTree Radius_tree("Radius", "Radius");
      double R;
      Radius_tree.Branch("R", &R);
      for(unsigned int j = 0; j < Radius.size(); ++j)
      {
        R = Radius[j];
        Radius_tree.Fill();
      }
      Radius_tree.ResetBranchAddresses();

      RooRealVar R_var("R","R",0,max);
      RooDataSet ds("ds","ds",RooArgSet(R_var),RooFit::Import(Radius_tree));

      RooRealVar gauss_mean("mean", "mean", maxX, 0.0, max);
      RooRealVar gauss_sigma("sigma", "sigma", Radius_hist.GetRMS()/8, 0.0, Radius_hist.GetRMS());
      RooRealVar bg_sigma("bg_sigma", "bg_sigma", Radius_hist.GetRMS()/3, Radius_hist.GetRMS()/5, max/2);
      RooGaussian gauss("gauss", "gauss", R_var, gauss_mean, gauss_sigma);
      RooGaussian background("background", "background", R_var, gauss_mean, bg_sigma);

      RooRealVar contrib("contrib", "contrib", 0.5, 0.0, 1.0);
      RooAddPdf model("model", "model", RooArgList(gauss, background), contrib);

      std::stringstream temp_ss;
      temp_ss << "Starting Value: " << gauss_mean.getVal() << "; ";

      //RooFitResult* result = gauss.fitTo(ds, RooFit::Save());

      {
        RooPlot* frame = R_var.frame(RooFit::Title(title.c_str()));
        ds.plotOn(frame);
        gauss.plotOn(frame, RooFit::LineColor(kGreen));
        background.plotOn(frame, RooFit::LineColor(kRed));
        model.plotOn(frame);
        frame->Draw();
        c1.SaveAs(("Plots/"+dirname+"_RadiusPreFit.png").c_str());
        c1.SaveAs(("Plots/"+dirname+"_RadiusPreFit.pdf").c_str());
      }

      /*bg_sigma.setConstant(kTRUE);
      model.fitTo(ds);
      bg_sigma.setConstant(kFALSE);
      {
        RooPlot* frame = R_var.frame(RooFit::Title(title.c_str()));
        ds.plotOn(frame);
        gauss.plotOn(frame, RooFit::LineColor(kGreen));
        background.plotOn(frame, RooFit::LineColor(kRed));
        model.plotOn(frame);
        frame->Draw();
        c1.SaveAs(("Plots/"+dirname+"_RadiusPostFit1.png").c_str());
        c1.SaveAs(("Plots/"+dirname+"_RadiusPostFit1.pdf").c_str());
      }// */

      /*gauss_sigma.setConstant(kTRUE);
      model.fitTo(ds);
      gauss_sigma.setConstant(kFALSE);
      {
        RooPlot* frame = R_var.frame(RooFit::Title(title.c_str()));
        ds.plotOn(frame);
        gauss.plotOn(frame, RooFit::LineColor(kGreen));
        background.plotOn(frame, RooFit::LineColor(kRed));
        model.plotOn(frame);
        frame->Draw();
        c1.SaveAs(("Plots/"+dirname+"_RadiusPostFit2.png").c_str());
        c1.SaveAs(("Plots/"+dirname+"_RadiusPostFit2.pdf").c_str());
      }// */

      RooFitResult* result = model.fitTo(ds, RooFit::Save());

      temp_ss << "Fitted Value: " << gauss_mean.getVal() << "; gaussian ratio: " << contrib.getVal() << std::endl;
      std::cout << temp_ss.str();

      {
        RooPlot* frame = R_var.frame(RooFit::Title(title.c_str()));
        ds.plotOn(frame);
        gauss.plotOn(frame, RooFit::LineColor(kGreen));
        background.plotOn(frame, RooFit::LineColor(kRed));
        model.plotOn(frame);
        frame->Draw();
        c1.SaveAs(("Plots/"+dirname+"_RadiusFit.png").c_str());
        c1.SaveAs(("Plots/"+dirname+"_RadiusFit.pdf").c_str());
      }

      std::cout << "Computing magnetic field: ";
      double B;
      {
        B = (32 * TMath::Pi() * 320)/(5 * sqrt(5)) * (coilA)/(0.068);
        B = B/10000000;
      }
      std::cout << B << " T" << std::endl;

      std::cout << "Charge to mass ratio: ";
      double chargeMass;
      {
        double R = gauss_mean.getVal()/100; // Convert from cm to meters
        chargeMass = 2*beamE*1000/(B*B * R*R);
      }
      std::cout << chargeMass << std::endl;

      results << "Run_" << itoa_len(i,3) << ": " << chargeMass << " (Curvature in B field)" << std::endl;
    }

    if(i == 62 || i == 63 || i == 64)
    // The run where E field force balances the B field force
    {
      std::cout << "Computing magnetic field: ";
      double B;
      {
        B = (32 * TMath::Pi() * 320)/(5 * sqrt(5)) * (coilA)/(0.068);
        B = B/10000000;
      }
      std::cout << B << " T" << std::endl;

      std::cout << "Charge to mass ratio: ";
      double dist = screenHeight / 100.;
      double chargeMass = 2 * B*B * dist*dist * beamE*1000;
      chargeMass = (plateV*1000*plateV*1000) / chargeMass;
      std::cout << chargeMass << std::endl;

      results << "Run_" << itoa_len(i,3) << ": " << chargeMass << " (E and B in equilibrium)" << std::endl;
    }
  }

  for(int i = 0; i <= MAX_RUNS; ++i)
  {
    file.cd();
    std::string dirname = "Run_";
    dirname += itoa_len(i, 3);

    // Simulating a lens focusing the image on a second screen
    // The lens has a given focal distance and the screen is calculated
    // to be at the distance necessary to obtain an image.
    // The focused image will be saved in the Sensel folder
    // The lens is assumed to have the same focal distance on both sides
    // As a first approx the screen will have the same dimensions as the
    // fluorescent screen
    double focalDist = 10.; //10 cm
    double fluoDist = 13.; //25 cm, approximate distance to fluorescent screen
    if(focalDist >= fluoDist)
      focalDist = fluoDist * 0.9;
    double magnification = focalDist/(focalDist-fluoDist);
    double screenDist = -fluoDist * magnification;
    //screenDist = screenDist*1.04;
    // To see effects of defocusing, change screen distance

    TDirectory* dir = file.GetDirectory(dirname.c_str());
    if(dir == NULL)
      continue;
    dir->cd();

    std::string title;
    double beamE, plateV;
    double coilA;
    double screenWidth, screenHeight, screenAngle;
    bool realisticFields;

    TVectorD* StatusVec;
    dir->GetObject("StatusVec", StatusVec);
    beamE = (double)(*StatusVec)[0];  // keV
    plateV = (double)(*StatusVec)[1]; // kV
    coilA = (double)(*StatusVec)[2];  // A
    screenWidth = (double)(*StatusVec)[3]; // cm
    screenHeight = (double)(*StatusVec)[4]; // cm
    screenAngle = (double)(*StatusVec)[5]; // deg
    if((double)(*StatusVec)[6] < 0)
      realisticFields = false;
    else
      realisticFields = true;

    title = ftoa_fixed_precision(beamE, 2)+"keV:"+ftoa_fixed_precision(plateV, 2)+"kV:"+ftoa_fixed_precision(coilA, 2)+"A";
    std::cout << "  " << title << std::endl;

    TTree* EyeHits;
    dir->GetObject("EyeHits", EyeHits);
    if(EyeHits == NULL)
    {
      std::cout << "Unable to find EyeHits tree for Run " << i << std::endl;
    }
    else
    {
      std::cout << "Run " << i << ": " << EyeHits->GetEntries() << " Eye Hits" << std::endl;
      if(EyeHits->GetEntries() == 0)
        continue;

      //int event;
      double LocalX, LocalY, LocalZ;
      //double GlobalX, GlobalY, GlobalZ;
      double Energy;
      double DirectionX, DirectionY, DirectionZ;

      //EyeHits->SetBranchAddress("Event", &evt);
      EyeHits->SetBranchAddress("LocalX", &LocalX);
      EyeHits->SetBranchAddress("LocalY", &LocalY);
      EyeHits->SetBranchAddress("LocalZ", &LocalZ);
      //EyeHits->SetBranchAddress("GlobalX", &val);
      //EyeHits->SetBranchAddress("GlobalY", &val);
      //EyeHits->SetBranchAddress("GlobalZ", &val);
      EyeHits->SetBranchAddress("Energy", &Energy);
      //EyeHits->SetBranchAddress("DirectionX", &val);
      //EyeHits->SetBranchAddress("DirectionY", &val);
      //EyeHits->SetBranchAddress("DirectionZ", &val);
      EyeHits->SetBranchAddress("LocalDirectionX", &DirectionX);
      EyeHits->SetBranchAddress("LocalDirectionY", &DirectionY);
      EyeHits->SetBranchAddress("LocalDirectionZ", &DirectionZ);

      std::vector<double> HitX;
      std::vector<double> HitY;

      for(unsigned int j = 0; j < EyeHits->GetEntries(); ++j)
      {
        EyeHits->GetEntry(j);


        double alpha = (focalDist - LocalZ)/DirectionZ;
        double focalX = LocalX + alpha * DirectionX;
        double focalY = LocalY + alpha * DirectionY;
        double beta = (LocalZ + screenDist)/focalDist;
        double screenX = LocalX - beta*focalX;
        double screenY = LocalY - beta*focalY;

        HitX.push_back(screenX);
        HitY.push_back(screenY);

        if(VERBOSE)
        {
          std::cout << "  Hit " << j << std::endl;
          std::cout << "   Position: (" << LocalX << ", " << LocalY << ", " << LocalZ << ")" << std::endl;
          std::cout << "   Vector: (" << DirectionX << ", " << DirectionY << ", " << DirectionZ << ")" << std::endl;
          std::cout << "   Energy: " << Energy << std::endl;
          std::cout << "   Screen Distance: " << screenDist << std::endl;
          std::cout << "   Alpha: " << alpha << std::endl;
          std::cout << "   Focal Position: (" << focalX << ", " << focalY << ")" << std::endl;
          std::cout << "   Aux Parallel Ray, refracted vector: (" << -focalX << ", " << -focalY << ", " << -focalDist << ")" << std::endl;
          std::cout << "   Beta: " << beta << std::endl;
          std::cout << "   Screen Hit: (" << screenX << ", " << screenY << ")" << std::endl;
        }
      }

      // Remember, magnification is negative!
      TH2F hist((dirname+"_hist").c_str(), (title+";x(cm);y(cm)").c_str(), 100, screenWidth/2*magnification, -screenWidth/2*magnification, 100, screenHeight/2*magnification, -screenHeight/2*magnification);
      TGraph* graph = NULL;
      graph = new TGraph(EyeHits->GetEntries(), &HitX[0], &HitY[0]);

      gStyle->SetOptStat(0);
      hist.Draw();
      graph->Draw("P");

      c1.SaveAs(("Sensel/"+dirname+".png").c_str());
      c1.SaveAs(("Sensel/"+dirname+".pdf").c_str());
    }
  }

  std::cout << results.str();
}
