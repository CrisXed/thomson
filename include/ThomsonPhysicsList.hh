#ifndef THOMSON_PHYSICSLIST_HH
#define THOMSON_PHYSICSLIST_HH

#include "G4VUserPhysicsList.hh"
#include "globals.hh"
#include "G4VPhysicsConstructor.hh"

class G4Scintillation;
class G4OpAbsorption;
//class G4OpRayleigh;
//class G4OpMieHG;
class G4OpBoundaryProcess;
//class G4LowEnergyBremsstrahlung;

class ThomsonPhysicsList: public G4VUserPhysicsList
{
public:
  // Constructor
  ThomsonPhysicsList(double scintillationYield);

  // Destructor
  virtual ~ThomsonPhysicsList();

protected:
  // Construct particles and physics processes
  void ConstructParticle();
  void ConstructProcess();

  void SetCuts();

private:
  // Helper methods
  void ConstructGeneral();
  void ConstructEM();

  G4double fCutForGamma;
  double scintillationYield_;

  G4VPhysicsConstructor* EMPhysicsList;

  G4Scintillation*     theScintillationProcess;

  G4OpAbsorption*      theAbsorptionProcess;
  //G4OpRayleigh*        theRayleighScatteringProcess;
  //G4OpMieHG*           theMieHGScatteringProcess;
  G4OpBoundaryProcess* theBoundaryProcess;

  //G4LowEnergyBremsstrahlung LeBrprocess;
};

#endif
