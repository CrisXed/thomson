#ifndef ThomsonGlobals_h
#define ThomsonGlobals_h 1

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

G4double PlateVoltage = 0*CLHEP::kilovolt;
G4double CoilCurrent = 0.25*CLHEP::ampere;
G4double BeamVoltage = 3*CLHEP::kilovolt;
G4double BulbDiameter = 13*CLHEP::cm;
G4double RibbonWidth;
bool OnlyRootOutput = false;
G4double ScreenWidth = 9*CLHEP::cm;
G4double ScreenHeight = 5.4*CLHEP::cm;
G4double ScreenAngle = 15*CLHEP::deg;
bool RealisticFields = false;
#endif
