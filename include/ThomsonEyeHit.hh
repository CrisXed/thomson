#ifndef ThomsonEyeHit_h
#define ThomsonEyeHit_h 1

#include "G4VHit.hh"
#include "G4Allocator.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh"

class ThomsonEyeHit : public G4VHit {
public:
  ThomsonEyeHit();

  ~ThomsonEyeHit();

  void Print();

  inline void *operator    new(size_t);
  inline void  operator delete(void *aHit);

  void SetWorldPos(G4ThreeVector xyz){WorldPos=xyz;}
  void SetLocalPos(G4ThreeVector xyz){LocalPos=xyz;}
  void SetEnergy(G4double e){Energy=e;}
  void SetDirection(G4ThreeVector xyz){Direction=xyz;}
  void SetLocalDirection(G4ThreeVector xyz){LocalDirection=xyz;}

  G4ThreeVector GetWorldPos() const{ return WorldPos;}
  G4ThreeVector GetLocalPos() const{ return LocalPos;}
  G4double GetEnergy() const{ return Energy;}
  G4ThreeVector GetDirection() const{ return Direction;}
  G4ThreeVector GetLocalDirection() const{ return LocalDirection;}

private:
  G4ThreeVector WorldPos;
  G4ThreeVector LocalPos;
  G4double      Energy;
  G4ThreeVector Direction;
  G4ThreeVector LocalDirection;
};

// Define the "hit collection" using the template class G4THitsCollection:
typedef G4THitsCollection<ThomsonEyeHit> ThomsonEyeHitCollection;


// -- new and delete overloaded operators:
extern G4Allocator<ThomsonEyeHit> ThomsonEyeHitAllocator;

inline void* ThomsonEyeHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) ThomsonEyeHitAllocator.MallocSingle();
  return aHit;
}

inline void ThomsonEyeHit::operator delete(void *aHit)
{
  ThomsonEyeHitAllocator.FreeSingle((ThomsonEyeHit*) aHit);
}

#endif
