#ifndef THOMSON_DETECTORMESSENGER_HH
#define THOMSON_DETECTORMESSENGER_HH 1

#include "G4UImessenger.hh"
#include "ThomsonDetectorConstruction.hh"
#include "globals.hh"

class G4UIcmdWithADoubleAndUnit;
class ThomsonDetectorConstruction;

class ThomsonDetectorMessenger: public G4UImessenger
{
public:
  ThomsonDetectorMessenger(ThomsonDetectorConstruction* detCons);
  ~ThomsonDetectorMessenger();

  void SetNewValue(G4UIcommand * command,G4String newValues);
  G4String GetCurrentValue(G4UIcommand * command);

private:
  ThomsonDetectorConstruction* detectorConstruction;

  G4UIdirectory* thomsonDirectory;
  G4UIcmdWithADoubleAndUnit* plateVoltageCmd;
  G4UIcmdWithADoubleAndUnit* coilCurrentCmd;
  G4UIcmdWithADoubleAndUnit* beamVoltageCmd;

protected:

};

#endif

