#ifndef ThomsonEyeSD_h
#define ThomsonEyeSD_h 1

#include "ThomsonEyeHit.hh"

#include "G4VSensitiveDetector.hh"

class G4Step;
class G4TouchableHistory;
class G4HCofThisEvent;


class ThomsonEyeSD : public G4VSensitiveDetector
{
  public:
    ThomsonEyeSD(G4String SDname);
    ~ThomsonEyeSD();

    G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist);

    void Initialize(G4HCofThisEvent* HCE);

    void EndOfEvent(G4HCofThisEvent* HCE);

  private:
    ThomsonEyeHitCollection* HitCollection;
};

#endif
