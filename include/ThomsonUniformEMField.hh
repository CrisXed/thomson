#ifndef THOMSON_UNIFORM_EMFIELD_HH
#define THOMSON_UNIFORM_EMFIELD_HH

#include "G4ElectroMagneticField.hh"
#include "G4ThreeVector.hh"

class ThomsonUniformEMField : public G4ElectroMagneticField
{
public:
  ThomsonUniformEMField();
  ThomsonUniformEMField(G4ThreeVector& B, G4ThreeVector& E);
  ThomsonUniformEMField(G4double Bx, G4double By, G4double Bz, G4double Ex, G4double Ey, G4double Ez);
  virtual ~ThomsonUniformEMField();

  virtual void GetFieldValue(const G4double Point[4], G4double *Bfield) const;

  virtual G4bool DoesFieldChangeEnergy() const;

  void SetFields(G4double Bx, G4double By, G4double Bz, G4double Ex, G4double Ey, G4double Ez);
  void SetFields(G4ThreeVector& B, G4ThreeVector& E){_Bfield=B; _Efield=E; return;}

  void SetEField(G4double Ex, G4double Ey, G4double Ez);
  void SetEfield(G4ThreeVector& E){_Efield=E; return;}

  void SetBField(G4double Bx, G4double By, G4double Bz);
  void SetBfield(G4ThreeVector& B){_Bfield=B; return;}

protected:

private:
  G4ThreeVector _Bfield;
  G4ThreeVector _Efield;
};

#endif
