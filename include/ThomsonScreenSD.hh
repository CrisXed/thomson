#ifndef ThomsonScreenSD_h
#define ThomsonScreenSD_h 1

#include "ThomsonScreenHit.hh"

#include "G4VSensitiveDetector.hh"

class G4Step;
class G4TouchableHistory;
class G4HCofThisEvent;


class ThomsonScreenSD : public G4VSensitiveDetector
{
  public:
    ThomsonScreenSD(G4String SDname);
    ~ThomsonScreenSD();

    G4bool ProcessHits(G4Step* aStep, G4TouchableHistory* ROhist);

    void Initialize(G4HCofThisEvent* HCE);

    void EndOfEvent(G4HCofThisEvent* HCE);

  private:
    ThomsonScreenHitCollection* HitCollection;
};

#endif
