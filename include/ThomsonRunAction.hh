#ifndef ThomsonRunAction_h
#define ThomsonRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <string>

class G4Run;
class TFile;
class TDirectory;
class TTree;

class ThomsonRunAction : public G4UserRunAction
{
  public:
    ThomsonRunAction();
    ~ThomsonRunAction();

    void BeginOfRunAction(const G4Run*);
    void EndOfRunAction(const G4Run*);

    std::string getFilename() const{return filename;}
    G4int getRunID() const{return runID;}

    std::ofstream* outfile;
    TFile* outfile_root;
    TTree* outtree;
    TTree* eyetree;

  protected:
  private:
    std::string filename;
    G4int runID;
    TDirectory* CurrentDir;
    TDirectory* RunDir;
};

#endif

