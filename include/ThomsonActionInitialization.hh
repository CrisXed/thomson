#ifndef ThomsonActionInitialization_h
#define ThomsonActionInitialization_h 1

#include "G4VUserActionInitialization.hh"

/// Action initialization class.

class ThomsonActionInitialization : public G4VUserActionInitialization
{
  public:
    ThomsonActionInitialization();
    virtual ~ThomsonActionInitialization();

    virtual void BuildForMaster() const;
    virtual void Build() const;
};

#endif
