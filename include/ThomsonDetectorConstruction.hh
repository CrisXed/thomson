#ifndef THOMSON_DETECTORCONSTRUCTION_HH
#define THOMSON_DETECTORCONSTRUCTION_HH

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

class G4LogicalVolume;
class G4VPhysicalVolume;
class ThomsonUniformEMField;

class ThomsonDetectorConstruction : public G4VUserDetectorConstruction
{
public:
  // Constructor
  ThomsonDetectorConstruction(G4double bulbDiameter = 13*CLHEP::cm, G4double neckDiameter = 3.8*CLHEP::cm, G4double neckLength = 13*CLHEP::cm, G4double screenHeight = 5.4*CLHEP::cm, G4double screenWidth = 9*CLHEP::cm, G4double screenAngle = 15*CLHEP::deg, bool realisticFields = false, bool noGlass = false);

  // Destructor
  virtual ~ThomsonDetectorConstruction();

  // Method
  virtual G4VPhysicalVolume* Construct();

  void SetPlateVoltage(G4double voltage);
  void SetCoilCurrent(G4double current);

protected:

private:
  // Helper methods
  void DefineMaterials();
  void SetupGeometry();

  // World logical and physical volumes
  G4LogicalVolume*   fpWorldLogical;
  G4VPhysicalVolume* fpWorldPhysical;

  G4double bulbDiameter_;
  G4double neckDiameter_;
  G4double neckLength_;
  G4double screenHeight_;
  G4double screenWidth_;
  G4double screenAngle_;
  bool realisticFields_;
  bool noGlass_;

  ThomsonUniformEMField* my_field;

};

#endif
