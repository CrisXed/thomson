#ifndef THOMSON_PRIMARYGENERATORACTION_HH
#define THOMSON_PRIMARYGENERATORACTION_HH

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ThreeVector.hh"
#include "globals.hh"

class G4ParticleGun;
class G4Event;

class ThomsonPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
  // Constructor
  ThomsonPrimaryGeneratorAction();

  // Destructor
  virtual ~ThomsonPrimaryGeneratorAction();

  // Method
  void GeneratePrimaries(G4Event*);

protected:

private:
  // Data member
  G4ParticleGun* particleGun;
};

#endif
