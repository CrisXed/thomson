#ifndef ThomsonEventAction_h
#define ThomsonEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"

class ThomsonRunAction;

class ThomsonEventAction : public G4UserEventAction
{
  public:
    ThomsonEventAction();
    ~ThomsonEventAction();

    void BeginOfEventAction(const G4Event*);
    void EndOfEventAction(const G4Event*);

  private:
    G4int ThomsonCollID;
    G4int ThomsonEyeCollID;
    ThomsonRunAction* aRun;
//    std::string fOutfile;
};

#endif

