#ifndef ThomsonScreenHit_h
#define ThomsonScreenHit_h 1

#include "G4VHit.hh"
#include "G4Allocator.hh"
#include "G4THitsCollection.hh"
#include "G4ThreeVector.hh"

class ThomsonScreenHit : public G4VHit {
public:
  ThomsonScreenHit();

  ~ThomsonScreenHit();

  void Print();

  inline void *operator    new(size_t);
  inline void  operator delete(void *aHit);

  //void          AddEdep(const double e){ eDep += e; }
  //void SetEDep (G4double de) {HitEdep=de;}
  //void SetEKin (G4double de) {HitEKin=de;}
  void SetWorldPos(G4ThreeVector xyz){WorldPos=xyz;}
  void SetLocalPos(G4ThreeVector xyz){LocalPos=xyz;}
  //void SetHitID (G4int ID){HitID=ID;}

  //G4double GetEdep() const { return HitEdep;}
  //G4double GetEKin() const { return HitEKin;}
  G4ThreeVector GetWorldPos() const{ return WorldPos;}
  G4ThreeVector GetLocalPos() const{ return LocalPos;}
  //G4int GetHitID () const {return HitID;}

private:
  //G4double      HitEdep;
  //G4double      HitEKin;
  G4ThreeVector WorldPos;
  G4ThreeVector LocalPos;
  //G4int HitID;
};

// Define the "hit collection" using the template class G4THitsCollection:
typedef G4THitsCollection<ThomsonScreenHit> ThomsonScreenHitCollection;


// -- new and delete overloaded operators:
extern G4Allocator<ThomsonScreenHit> ThomsonScreenHitAllocator;

inline void* ThomsonScreenHit::operator new(size_t)
{
  void *aHit;
  aHit = (void *) ThomsonScreenHitAllocator.MallocSingle();
  return aHit;
}
inline void ThomsonScreenHit::operator delete(void *aHit)
{
  ThomsonScreenHitAllocator.FreeSingle((ThomsonScreenHit*) aHit);
}

#endif
