#include "ThomsonDetectorConstruction.hh"
#include "ThomsonUniformEMField.hh"
#include "ThomsonScreenSD.hh"
#include "ThomsonEyeSD.hh"

#include "G4PVPlacement.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Material.hh"
#include "G4Element.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4NistManager.hh"
#include "G4VisAttributes.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4EqMagElectricField.hh"
#include "G4MagIntegratorStepper.hh"
#include "G4ClassicalRK4.hh"
#include "G4ChordFinder.hh"
#include "G4SDManager.hh"

#define SURFACE_CHECK false

extern G4double PlateVoltage;
extern G4double CoilCurrent;
extern G4double BulbDiameter;
extern G4double RibbonWidth;
extern G4double ScreenWidth;
extern G4double ScreenHeight;
extern G4double ScreenAngle;
extern bool RealisticFields;

ThomsonDetectorConstruction::ThomsonDetectorConstruction(G4double bulbDiameter, G4double neckDiameter, G4double neckLength, G4double screenHeight, G4double screenWidth, G4double screenAngle, bool realisticFields, bool noGlass)
  :fpWorldLogical(0)
  ,fpWorldPhysical(0)
  ,bulbDiameter_(bulbDiameter)
  ,neckDiameter_(neckDiameter)
  ,neckLength_(neckLength)
  ,screenHeight_(screenHeight)
  ,screenWidth_(screenWidth)
  ,screenAngle_(screenAngle)
  ,realisticFields_(realisticFields)
  ,noGlass_(noGlass)
{
  using namespace CLHEP;

  G4double depth = tan(screenAngle_/rad) * screenWidth;
  G4double size = sqrt((screenWidth+0.15*mm)*(screenWidth+0.15*mm) + (screenHeight+1*mm)*(screenHeight+1*mm) + (depth+0.15*mm)*(depth+0.15*mm));
  if(size > bulbDiameter_)
  {
    G4cout << "The selected size for the screen is larger than the tube." << std::endl;
    G4cout << "The size of the screen will be scaled down, maintaining the relative proportions, so that it fits" << std::endl;

    G4double scale = (bulbDiameter_*0.9) / size;
    screenWidth *= scale;
    screenHeight *= scale;
    depth *= scale;
    screenWidth_ = screenWidth;
    screenHeight_ = screenHeight;
  }

  RibbonWidth = depth+0.2*mm;

  if(neckDiameter_ < RibbonWidth)
  {
    G4cout << "The diameter of the neck is smaller the the necessary width for the ribbon beam of electrons to cover the full screen." << std::endl;
    G4cout << "The diameter of the neck will be increased to accomadate for the width of the beam of electrons" << std::endl;
    G4cout << "The diameter of the neck is changed from " << neckDiameter_ << " to ";
    neckDiameter_ = RibbonWidth * 1.1;
    G4cout << neckDiameter_ << std::endl;
  }

  BulbDiameter = bulbDiameter_;
  ScreenWidth = screenWidth_;
  ScreenHeight = screenHeight_;
  ScreenAngle = screenAngle_;
  RealisticFields = realisticFields_;
}

ThomsonDetectorConstruction::~ThomsonDetectorConstruction() {}

G4VPhysicalVolume* ThomsonDetectorConstruction::Construct()
{
  // Material Definition
  DefineMaterials();

  // Geometry Definition
  SetupGeometry();

  // Return world volume
  return fpWorldPhysical;
}

void ThomsonDetectorConstruction::DefineMaterials()
{
  using namespace CLHEP;

  G4String symbol;
  //G4double a, z;
  G4double density;
  G4int ncomponents;
  //G4int natoms;
  G4double fractionmass, totalMass;
  totalMass = 0.0*g/mole;

  G4NistManager* Nist = G4NistManager::Instance();
  //G4Material* K = Nist->FindOrBuildMaterial("G4_K");
  //G4Material* Ca = Nist->FindOrBuildMaterial("G4_Ca");
  //G4Material* W = Nist->FindOrBuildMaterial("G4_W");
  //G4Material* O = Nist->FindOrBuildMaterial("G4_O");
  //G4Material* Al = Nist->FindOrBuildMaterial("G4_Al");
  //G4Material* Si = Nist->FindOrBuildMaterial("G4_Si");

  //http://en.wikipedia.org/wiki/Potassium_oxide
  /*density = 2.35*g/cm3;
  G4Material* K2O = new G4Material("K2O", density, ncomponents=2);
  K2O->AddMaterial(K, natoms=2);
  K2O->AddMaterial(O, natoms=1);// */
  G4Material* K2O = Nist->FindOrBuildMaterial("G4_POTASSIUM_OXIDE");
  totalMass += 94.20*g/mole;

  //http://en.wikipedia.org/wiki/Al2O3
  /*density = 4.0*g/cm3;
  G4Material* Al2O3 = new G4Material("Al2O3", density, ncomponents=2);
  Al2O3->AddMaterial(Al, natoms=2);
  Al2O3->AddMaterial(O, natoms=3);// */
  G4Material* Al2O3 = Nist->FindOrBuildMaterial("G4_ALUMINUM_OXIDE");
  totalMass += 101.96*g/mole;

  //http://en.wikipedia.org/wiki/SiO2
  /*density = 2.648*g/cm3;
  G4Material* SiO2 = new G4Material("SiO2", density, ncomponents=2);
  SiO2->AddMaterial(Si, natoms=1);
  SiO2->AddMaterial(O, natoms=2);// */
  G4Material* SiO2 = Nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE");
  totalMass += 60.08*g/mole;

  //http://www.2spi.com/catalog/submat/mic_shet.php
  G4Material* Mica = new G4Material("Mica", density=2.7*g/cm3, ncomponents=3);
  Mica->AddMaterial(SiO2, fractionmass=(60.08*g/mole)/totalMass);
  Mica->AddMaterial(Al2O3, fractionmass=(101.96*g/mole)/totalMass);
  Mica->AddMaterial(K2O, fractionmass=(94.20*g/mole)/totalMass);
  // Will use http://en.wikipedia.org/wiki/Muscovite for other properties

  //https://archive.org/details/spectralemission417shel
  //http://scintillator.lbl.gov/
  //http://refractiveindex.info/?group=CRYSTALS&material=CaWO4
  //DOI: 10.1103/PhysRevB.75.184308
  //Combining info from all for CaWO4 aka Calcium Tungstate

  //http://books.google.pt/books?id=rtDeU5hxsfYC&pg=PA54&lpg=PA54&dq=CaWO4+refractive+index&source=bl&ots=63VsVqn4XZ&sig=agIfx7uCsaVq-CEpfm4Oz0xV_Ug&hl=en&sa=X&ei=j0PhUsOYGYbQ7Aaoq4HgDw&ved=0CFcQ6AEwBg#v=onepage&q=CaWO4%20refractive%20index&f=false
  G4Material* Phosphor = Nist->FindOrBuildMaterial("G4_CALCIUM_TUNGSTATE");

  const G4int NUMENTRIES = 16;
  G4double wavelength[NUMENTRIES] = {380*nanometer, 400*nanometer, 420*nanometer, 440*nanometer, 460*nanometer, 480*nanometer, 500*nanometer, 520*nanometer, 540*nanometer, 560*nanometer, 580*nanometer, 600*nanometer, 620*nanometer, 640*nanometer, 660*nanometer, 680*nanometer};
  G4double ppckov[NUMENTRIES];
  G4double rindex[NUMENTRIES];
  G4double air_rindex[NUMENTRIES];
  G4double air_abslength[NUMENTRIES];
  G4double glass_rindex[NUMENTRIES];
  G4double glass_abslength[NUMENTRIES];
  G4double mica_rindex[NUMENTRIES];
  G4double mica_abslength[NUMENTRIES];
  // eV = 1240/nm
  // n = sqrt( 1 + 2.5493*pow(x,2)/(pow(x,2)-pow(0.1347,2)) + 0.9200*pow(x,2)/(pow(x,2)-pow(10.815,2)) );

  G4double Scnt_FAST[NUMENTRIES] = {0.36, 0.62, 0.884, 1.000, 0.945, 0.794, 0.615, 0.447, 0.310, 0.207, 0.137, 0.093, 0.061, 0.038, 0.026, 0.016};
  G4double Scnt_SLOW[NUMENTRIES] = {0.36, 0.62, 0.884, 1.000, 0.945, 0.794, 0.615, 0.447, 0.310, 0.207, 0.137, 0.093, 0.061, 0.038, 0.026, 0.016};

  for(int i = 0; i < NUMENTRIES; ++i)
  {
    G4double wavlen = wavelength[i]/nanometer;

    ppckov[i] = (1240./wavlen)*eV;

    double x = wavlen*nanometer/micrometer;
    rindex[i] = sqrt( 1 + 2.5493*pow(x,2)/(pow(x,2)-pow(0.1347,2)) + 0.9200*pow(x,2)/(pow(x,2)-pow(10.815,2)) );
    //G4cout << "Index of refraction for " << wavlen << " nm - " << x << " um: " << rindex[i] << G4endl;
    air_rindex[i] = 1.;
    air_abslength[i] = 1000*m;
    //http://en.wikipedia.org/wiki/Borosilicate_glass
    glass_rindex[i] = 1.52;
    glass_abslength[i] = 18.98*mm;
    mica_rindex[i] = 1.58;
    mica_abslength[i] = 3.*mm; //Completely invented value, also using it for the phosphour
  }

  G4MaterialPropertiesTable* Scnt_MPT = new G4MaterialPropertiesTable();

  Scnt_MPT->AddConstProperty("YIELDRATIO", 0.3);
  Scnt_MPT->AddConstProperty("FASTTIMECONSTANT",  1400.*ns);
  Scnt_MPT->AddConstProperty("SLOWTIMECONSTANT", 9200.*ns);
  Scnt_MPT->AddConstProperty("RESOLUTIONSCALE", 1.0);  //Couldn't find this value in the literature
  Scnt_MPT->AddConstProperty("SCINTILLATIONYIELD", 15000./MeV);

  Scnt_MPT->AddProperty("FASTCOMPONENT", ppckov, Scnt_FAST, NUMENTRIES);
  Scnt_MPT->AddProperty("SLOWCOMPONENT", ppckov, Scnt_SLOW, NUMENTRIES);

  Scnt_MPT->AddProperty("RINDEX",ppckov,rindex,NUMENTRIES)->SetSpline(true);
  Scnt_MPT->AddProperty("ABSLENGTH",ppckov,mica_abslength,NUMENTRIES)->SetSpline(true);

  Phosphor->SetMaterialPropertiesTable(Scnt_MPT);

  G4Material* air = Nist->FindOrBuildMaterial("G4_AIR");
  G4Material* vacuum = Nist->FindOrBuildMaterial("G4_Galactic");
  G4MaterialPropertiesTable* Air_MPT = new G4MaterialPropertiesTable();
  Air_MPT->AddProperty("RINDEX",ppckov,air_rindex,NUMENTRIES)->SetSpline(true);
  Air_MPT->AddProperty("ABSLENGTH",ppckov,air_abslength,NUMENTRIES)->SetSpline(true);
  air->SetMaterialPropertiesTable(Air_MPT);
  vacuum->SetMaterialPropertiesTable(Air_MPT);

  //Use this to calculate ABSLENGTH:
  // http://www.pgo-online.com/intl/katalog/curves/pyrex_kurve.html
  //In the visible range, 2mm of pyrex absorbs 10% of light, putting those
  //values into an exponential decay formula, we get the 1/e photons are
  //absorbed in 18.98 mm of pyrex, we will use this as the ABSLENGTH
  G4Material* glass = Nist->FindOrBuildMaterial("G4_Pyrex_Glass");
  G4MaterialPropertiesTable* Glass_MPT = new G4MaterialPropertiesTable();
  Glass_MPT->AddProperty("RINDEX",ppckov,glass_rindex,NUMENTRIES)->SetSpline(true);
  Glass_MPT->AddProperty("ABSLENGTH",ppckov,glass_abslength,NUMENTRIES)->SetSpline(true);
  glass->SetMaterialPropertiesTable(Glass_MPT);

  G4MaterialPropertiesTable* Mica_MPT = new G4MaterialPropertiesTable();
  Mica_MPT->AddProperty("RINDEX",ppckov,mica_rindex,NUMENTRIES)->SetSpline(true);
  Mica_MPT->AddProperty("ABSLENGTH",ppckov,mica_abslength,NUMENTRIES)->SetSpline(true);
  Mica->SetMaterialPropertiesTable(Mica_MPT);


  //G4Material* air = new G4Material("Air", density= 1.290*mg/cm3, ncomponents=2);
  //air->AddElement(N, fractionmass=0.7);
  //air->AddElement(O, fractionmass=0.3);// */
}

void ThomsonDetectorConstruction::SetupGeometry()
{
  using namespace CLHEP;

  //G4Material* air = G4Material::GetMaterial("Air");
  G4NistManager* Nist = G4NistManager::Instance();
  G4Material* air = Nist->FindOrBuildMaterial("G4_AIR");
  G4Material* al = Nist->FindOrBuildMaterial("G4_Al");
  G4Material* glass;
  if(!noGlass_)
    glass = Nist->FindOrBuildMaterial("G4_Pyrex_Glass");
  else
    glass = Nist->FindOrBuildMaterial("G4_AIR");
  //G4Material* glass = Nist->FindOrBuildMaterial("G4_W");
  G4Material* vacuum = Nist->FindOrBuildMaterial("G4_Galactic");
  G4Material* mica = Nist->FindOrBuildMaterial("Mica");
  G4Material* phosphor = Nist->FindOrBuildMaterial("G4_CALCIUM_TUNGSTATE");
  //G4Material* mica = Nist->FindOrBuildMaterial("G4_GRAPHITE");


  G4double worldSize = 2.0*m;
  if (worldSize < bulbDiameter_)
    worldSize = bulbDiameter_ * 1.2;
  G4Box* worldSolid = new G4Box("World_Solid", worldSize, worldSize, worldSize);
  fpWorldLogical = new G4LogicalVolume(worldSolid, air, "World_Logical");
  fpWorldPhysical = new G4PVPlacement(0, G4ThreeVector(), fpWorldLogical, "World_Physical", 0, false, 0, SURFACE_CHECK);

  G4double glassThickness = 1*mm;
  G4double overprovision = 1*cm;
  if(overprovision > bulbDiameter_/2)
    overprovision = bulbDiameter_/2;

  G4RotationMatrix tubeRotate;
  tubeRotate.rotateY(90.*deg);
  G4double translate = bulbDiameter_/2 + neckLength_/2 - overprovision;
  G4ThreeVector translation(-translate, 0.*cm, 0.*cm);

  G4Sphere* glassSphere = new G4Sphere("Glass_Sphere", 0.*cm, bulbDiameter_/2+glassThickness, 0.*deg, 360.*deg, 0.*deg, 180.*deg);
  G4Tubs* glassTube = new G4Tubs("Glass_Tube", 0.*cm, neckDiameter_/2+glassThickness, neckLength_/2+glassThickness+overprovision, 0.*deg, 360.*deg);
  G4UnionSolid* tubeSolid = new G4UnionSolid("Tube_Solid", glassSphere, glassTube, &tubeRotate, translation);
  G4LogicalVolume* tubeLogical = new G4LogicalVolume(tubeSolid, glass, "Tube_Logical");
  //G4VPhysicalVolume* tubePhysical =
  new G4PVPlacement(0, G4ThreeVector(), tubeLogical, "Tube_Physical", fpWorldLogical, false, 0, SURFACE_CHECK);

  G4Sphere* vacuumSphere = new G4Sphere("Vacuum_Sphere", 0.*cm, bulbDiameter_/2, 0.*deg, 360.*deg, 0.*deg, 180.*deg);
  G4Tubs* vacuumTube = new G4Tubs("Vacuum_Tube", 0.*cm, neckDiameter_/2, neckLength_/2+overprovision, 0.*deg, 360.*deg);
  G4UnionSolid* vacuumSolid = new G4UnionSolid("Vacuum_Solid", vacuumSphere, vacuumTube, &tubeRotate, translation);
  G4LogicalVolume* vacuumLogical = new G4LogicalVolume(vacuumSolid, vacuum, "Vacuum_Logical");
  //G4VPhysicalVolume* vacuumPhysical =
  new G4PVPlacement(0, G4ThreeVector(), vacuumLogical, "Vacuum_Physical", tubeLogical, false, 0, SURFACE_CHECK);

  G4double screenWidth = screenWidth_;
  G4double screenHeight = screenHeight_;
  G4double depth = tan(screenAngle_/rad) * screenWidth;
  G4double size = sqrt((screenWidth+0.15*mm)*(screenWidth+0.15*mm) + (screenHeight+1*mm)*(screenHeight+1*mm) + (depth+0.15*mm)*(depth+0.15*mm));

  G4Box* targetSolid = new G4Box("Target_Solid", screenWidth/2+0.075*mm, screenHeight/2 + 0.5*mm, depth/2+0.075*mm);
  G4LogicalVolume* targetLogical = new G4LogicalVolume(targetSolid, vacuum, "Target_Logical");
  //G4VPhysicalVolume* targetPhysical =
  new G4PVPlacement(0, G4ThreeVector(), targetLogical, "Target_Physical", vacuumLogical, false, 0, SURFACE_CHECK);

  G4Box* plateSolid = new G4Box("Plate_Solid", screenWidth/2+0.075*mm, 0.25*mm, depth/2+0.075*mm);
  G4LogicalVolume* plateLogical = new G4LogicalVolume(plateSolid, al, "Plate_Logical");
  //G4VPhysicalVolume* upperPlatePhysical =
  new G4PVPlacement(0, G4ThreeVector(0.*cm,  (screenHeight/2 + 0.25*mm), 0.*cm), plateLogical, "Upper_Plate_Physical", targetLogical, false, 0, SURFACE_CHECK);
  //G4VPhysicalVolume* lowerPlatePhysical =
  new G4PVPlacement(0, G4ThreeVector(0.*cm, -(screenHeight/2 + 0.25*mm), 0.*cm), plateLogical, "Lower_Plate_Physical", targetLogical, false, 0, SURFACE_CHECK);

  G4double actualScreenWidth = screenWidth/cos(screenAngle_/rad);
  //Thickness from:
  //http://www.2spi.com/catalog/submat/mic_shet.php
  G4Box* micaSolid = new G4Box("Mica_Solid", actualScreenWidth/2, screenHeight/2, 0.075*mm);
  G4LogicalVolume* micaLogical = new G4LogicalVolume(micaSolid, mica, "Mica_Logical");
  G4RotationMatrix micaRotate;
  micaRotate.rotateY(screenAngle_);
  //G4VPhysicalVolume* micaPhysical =
  new G4PVPlacement(G4Transform3D(micaRotate, G4ThreeVector()), micaLogical, "Mica_Physical", targetLogical, false, 0, SURFACE_CHECK);

  // Get paint thickness
  G4Box* phosphorSolid = new G4Box("Phosphor_Solid", actualScreenWidth/2, screenHeight/2, 0.003*mm);
  G4LogicalVolume* phosphorLogical = new G4LogicalVolume(phosphorSolid, phosphor, "Phosphor_Logical");
  //G4VPhysicalVolume* phosphorPhysical =
  new G4PVPlacement(0, G4ThreeVector(0., 0., -0.072), phosphorLogical, "Phosphor_Physical", micaLogical, false, 0, SURFACE_CHECK);

  // Eye dimensions from: http://en.wikipedia.org/wiki/Human_eye#Structure
  G4Tubs* eyeSectionSolid = new G4Tubs("EyeSectionSolid", 0*mm, 12.0*mm, 0.001*mm, 0.*deg, 360.*deg );
  G4LogicalVolume* eyeSectionLogical = new G4LogicalVolume(eyeSectionSolid, air, "EyeSection_Logical");
  //G4VPhysicalVolume* eyeSectionPhysical =
  new G4PVPlacement(0, G4ThreeVector(0., 0., -bulbDiameter_), eyeSectionLogical, "EyeSection_Logical", fpWorldLogical, false, 0, SURFACE_CHECK);


  ////////////////////////////////////////////////////////////////////////
  G4int n = 320;
  G4double r = 0.068;
  G4double MagField = ( ( ((32*pi*n)/(5*std::sqrt(5))) * ((CoilCurrent/ampere)/r) ) / (10000000.) ) * tesla;
  my_field = new ThomsonUniformEMField(0.0, 0.0, MagField,
                                        0.0, (PlateVoltage)/(screenHeight), 0.0);

  G4FieldManager* targetFM = new G4FieldManager(my_field);
  G4EqMagElectricField* fEquation = new G4EqMagElectricField(my_field);
  G4MagIntegratorStepper* fStepper = new G4ClassicalRK4(fEquation, 8);
  G4MagInt_Driver* fIntgrDriver = new G4MagInt_Driver(0.01*mm, fStepper, fStepper->GetNumberOfVariables());
  G4ChordFinder* fChordFinder = new G4ChordFinder(fIntgrDriver);

  targetFM->SetChordFinder(fChordFinder);
  targetLogical->SetFieldManager(targetFM, true);


  ////////////////////////////////////////////////////////////////////////
  G4SDManager* SDManager = G4SDManager::GetSDMpointer();

  ThomsonScreenSD* MySD = new ThomsonScreenSD("Screen_Sensitive_Detector");
  ThomsonEyeSD* EyeSD = new ThomsonEyeSD("Eye_Sensitive_Detector");

  SDManager->AddNewDetector(MySD);
  SDManager->AddNewDetector(EyeSD);
  phosphorLogical->SetSensitiveDetector(MySD);
  eyeSectionLogical->SetSensitiveDetector(EyeSD);
  ////////////////////////////////////////////////////////////////////////


  // Visualisation attributes

  // Invisible world volume.
  fpWorldLogical->SetVisAttributes(G4VisAttributes::Invisible);

  // Tube Volume - light blue
  G4VisAttributes* tubeAttributes = new G4VisAttributes(G4Colour(0.0,0.5,0.5,0.3));
  //tubeAttributes->SetForceSolid(true);
  //tubeLogical->SetVisAttributes(tubeAttributes);
  //vacuumLogical->SetVisAttributes(G4VisAttributes::Invisible);
  tubeLogical->SetVisAttributes(G4VisAttributes::Invisible);
  vacuumLogical->SetVisAttributes(tubeAttributes);

  // Target Volume - invisible
  targetLogical->SetVisAttributes(G4VisAttributes::Invisible);

  // Plate Volume - Green
  G4VisAttributes* plateAttributes = new G4VisAttributes(G4Colour(0.0,0.5,0.0,0.7));
  //plateAttributes->SetForceSolid(true);
  plateLogical->SetVisAttributes(plateAttributes);

  // Mica Volume - Yellow
  G4VisAttributes* micaAttributes = new G4VisAttributes(G4Colour(0.5,0.5,0.0,0.7));
  //micaAttributes->SetForceSolid(true);
  micaLogical->SetVisAttributes(micaAttributes);
}

void ThomsonDetectorConstruction::SetPlateVoltage(G4double voltage)
{
  my_field->SetEField(0.0, (voltage)/(screenHeight_), 0.0);
}

void ThomsonDetectorConstruction::SetCoilCurrent(G4double current)
{
  using namespace CLHEP;
  G4int n = 320;
  G4double r = 0.068;
  G4double MagField = ( ( ((32*pi*n)/(5*std::sqrt(5))) * ((current/ampere)/r) ) / (10000000.) ) * tesla;
  my_field->SetBField(0.0, 0.0, MagField);
}
