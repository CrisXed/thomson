#include "ThomsonActionInitialization.hh"
#include "ThomsonPrimaryGeneratorAction.hh"
#include "ThomsonRunAction.hh"
#include "ThomsonEventAction.hh"

ThomsonActionInitialization::ThomsonActionInitialization()
 : G4VUserActionInitialization()
{}

ThomsonActionInitialization::~ThomsonActionInitialization()
{}

void ThomsonActionInitialization::BuildForMaster() const
{
  SetUserAction(new ThomsonRunAction);
}

void ThomsonActionInitialization::Build() const
{
  SetUserAction(new ThomsonPrimaryGeneratorAction);
  SetUserAction(new ThomsonRunAction);

  ThomsonEventAction* eventAction = new ThomsonEventAction;
  SetUserAction(eventAction);
}
