#include "ThomsonScreenSD.hh"
#include "G4TouchableHistory.hh"
#include "G4Step.hh"
#include "G4HCofThisEvent.hh"
#include "G4HCtable.hh"
#include "G4UnitsTable.hh"

#include "G4VProcess.hh"

extern G4double ScreenAngle;

ThomsonScreenSD::ThomsonScreenSD(G4String SDname)
: G4VSensitiveDetector(SDname)
{
  G4cout << "Creating SD with name: " << SDname << G4endl;

  collectionName.insert("ThomsonScreenHitCollection");
}

ThomsonScreenSD::~ThomsonScreenSD()
{
}

G4bool ThomsonScreenSD::ProcessHits(G4Step* aStep, G4TouchableHistory* )//ROhist
{
  /*G4cout << "Processing a step." << G4endl;
  G4cout << "\tEnergy Deposit: " << aStep->GetTotalEnergyDeposit()/keV << " keV" << G4endl;

  G4Track* aTrack = aStep->GetTrack();
  G4ParticleDefinition* aParticle = aTrack->GetDefinition();
  //G4int trackID = aTrack->GetTrackID();

  G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
  G4StepPoint* postStepPoint = aStep->GetPostStepPoint();
  G4ThreeVector Position = preStepPoint->GetPosition();
  G4TouchableHistory* theTouchable = (G4TouchableHistory*)(preStepPoint->GetTouchable());
  G4String thisVolume = theTouchable->GetVolume()->GetName();
  const G4VProcess* CurrentProcess = preStepPoint->GetProcessDefinedStep();
  const G4VProcess* PostProcess = postStepPoint->GetProcessDefinedStep();

  G4cout << "\tKinetic Energy: " << preStepPoint->GetKineticEnergy()/keV << " keV" << G4endl;
  G4cout << "\tIn volume " << thisVolume << G4endl;
  G4cout << "\tVolume Pos: " << aTrack->GetNextVolume()->GetName() << G4endl;
  G4cout << "\tStep Process: " << CurrentProcess->GetProcessName() << G4endl;
  G4cout << "\tPost Process: " << PostProcess->GetProcessName() << G4endl;
  //Use name to identify particles
  G4cout << "\tParticle Name: " << aParticle->GetParticleName() << G4endl;
  G4cout << "\tParticle Type: " << aParticle->GetParticleType() << G4endl;

  if(aStep->IsFirstStepInVolume() || preStepPoint->GetStepStatus() == fGeomBoundary)
    G4cout << "\tFirst Step in Volume" << G4endl;// */

  G4StepPoint* preStepPoint = aStep->GetPreStepPoint();
  G4Track* aTrack = aStep->GetTrack();
  G4ParticleDefinition* aParticle = aTrack->GetDefinition();

  if(preStepPoint->GetStepStatus() == fGeomBoundary && (aParticle->GetParticleName() == "e-" || aParticle->GetParticleName() == "e+"))
  {
    G4ThreeVector WorldPos = preStepPoint->GetPosition();
    G4TouchableHistory* theTouchable = (G4TouchableHistory*)(preStepPoint->GetTouchable());
    G4ThreeVector LocalPos = theTouchable->GetHistory()->GetTopTransform().TransformPoint(WorldPos);

    // Warping the x direction
    LocalPos.setX(cos(ScreenAngle) * LocalPos.getX());

    ThomsonScreenHit* newHit = new ThomsonScreenHit();
    newHit->SetWorldPos(WorldPos);
    newHit->SetLocalPos(LocalPos);

    HitCollection->insert(newHit);
  }

  return true;
}

void ThomsonScreenSD::Initialize(G4HCofThisEvent* HCE)
{
  HitCollection = new ThomsonScreenHitCollection (GetName(), "ThomsonScreenHitCollection");

  static G4int HCID = -1;

  if (HCID<0)
  {
    HCID = GetCollectionID(0);
  }

  HCE->AddHitsCollection(HCID, HitCollection);
}

void ThomsonScreenSD::EndOfEvent(G4HCofThisEvent*)
{
  //HitCollection->PrintAllHits();
}
