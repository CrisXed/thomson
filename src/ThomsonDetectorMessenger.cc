#include "ThomsonDetectorMessenger.hh"
#include "ThomsonDetectorConstruction.hh"

#include "G4UIcmdWithADoubleAndUnit.hh"

extern G4double PlateVoltage;
extern G4double CoilCurrent;
extern G4double BeamVoltage;

ThomsonDetectorMessenger::ThomsonDetectorMessenger(ThomsonDetectorConstruction* detCons)
:detectorConstruction(detCons)
{
  thomsonDirectory = new G4UIdirectory("/thomson/");
  thomsonDirectory->SetGuidance("Thomson Experiment control commands.");

  plateVoltageCmd = new G4UIcmdWithADoubleAndUnit("/thomson/plateVoltage", this);
  plateVoltageCmd->SetGuidance("Set voltage applied to the plates.");
  plateVoltageCmd->SetParameterName("Voltage", true, true);
  plateVoltageCmd->SetDefaultUnit("kilovolt");
  plateVoltageCmd->SetUnitCandidates("volt kilovolt");

  coilCurrentCmd = new G4UIcmdWithADoubleAndUnit("/thomson/coilCurrent", this);
  coilCurrentCmd->SetGuidance("Set current in the Helmholtz coils.");
  coilCurrentCmd->SetParameterName("Current", true, true);
  coilCurrentCmd->SetDefaultUnit("ampere");
  coilCurrentCmd->SetUnitCandidates("ampere");

  //G4UIcmdWithADoubleAndUnit* beamVoltageCmd;
  beamVoltageCmd = new G4UIcmdWithADoubleAndUnit("/thomson/beamVoltage", this);
  beamVoltageCmd->SetGuidance("Set voltage in the electron gun.");
  beamVoltageCmd->SetParameterName("Voltage", true, true);
  beamVoltageCmd->SetDefaultUnit("kilovolt");
  beamVoltageCmd->SetUnitCandidates("kilovolt");
}

ThomsonDetectorMessenger::~ThomsonDetectorMessenger()
{
  delete beamVoltageCmd;
  delete coilCurrentCmd;
  delete plateVoltageCmd;

  delete thomsonDirectory;
}

void ThomsonDetectorMessenger::SetNewValue(G4UIcommand* command,G4String newValues)
{
  if(command == plateVoltageCmd)
  {
    PlateVoltage = plateVoltageCmd->GetNewDoubleValue(newValues);
    detectorConstruction->SetPlateVoltage(PlateVoltage);
  }
  else if(command == coilCurrentCmd)
  {
    CoilCurrent = coilCurrentCmd->GetNewDoubleValue(newValues);
    detectorConstruction->SetCoilCurrent(CoilCurrent);
  }
  else if(command == beamVoltageCmd)
  {
    BeamVoltage = beamVoltageCmd->GetNewDoubleValue(newValues);
  }
}

G4String ThomsonDetectorMessenger::GetCurrentValue(G4UIcommand * command)
{
  G4String cv;

  if(command == plateVoltageCmd)
  {
    cv = plateVoltageCmd->ConvertToString(PlateVoltage, "kilovolt");
  }
  else if(command == coilCurrentCmd)
  {
    cv = coilCurrentCmd->ConvertToString(CoilCurrent, "ampere");
  }
  else if(command == beamVoltageCmd)
  {
    cv = beamVoltageCmd->ConvertToString(BeamVoltage, "kilovolt");
  }

  return cv;
}
