#include "ThomsonEventAction.hh"
#include "ThomsonRunAction.hh"
#include "ThomsonScreenHit.hh"
#include "ThomsonEyeHit.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

#include "TTree.h"

extern bool OnlyRootOutput;

ThomsonEventAction::ThomsonEventAction():  ThomsonCollID(-1)
{
  aRun = (ThomsonRunAction*)(G4RunManager::GetRunManager())->GetUserRunAction();
  ThomsonCollID = -1;
  ThomsonEyeCollID = -1;
}

ThomsonEventAction::~ThomsonEventAction()
{
}

void ThomsonEventAction::BeginOfEventAction(const G4Event*)
{
  G4SDManager * SDman = G4SDManager::GetSDMpointer();
  int loopCounter = 0;
  bool loop = true;

  if(ThomsonCollID>=0 && ThomsonEyeCollID>=0 && ThomsonCollID != ThomsonEyeCollID)
    loop = false;

  while(loop)
  {
    if(loopCounter > 0)
      SDman->ListTree();

    if(ThomsonCollID<0)
    {
      G4String colNam;
      ThomsonCollID = SDman->GetCollectionID(colNam="ThomsonScreenHitCollection");
    }
    if(ThomsonEyeCollID<0)
    {
      G4String colNam;
      ThomsonEyeCollID = SDman->GetCollectionID(colNam="ThomsonEyeHitCollection");
    }

    if(ThomsonCollID == ThomsonEyeCollID)
    {
      G4cout << "Thomson Screen Hit Collection ID: " << ThomsonCollID << G4endl;
      G4cout << "Thomson Eye Hit Collection ID: " << ThomsonEyeCollID << G4endl;
      ThomsonCollID = -1;
      ThomsonEyeCollID = -1;
      if(loopCounter > 5)
        loop = false;
    }
    else
      loop = false;

    ++loopCounter;
  }
}

void ThomsonEventAction::EndOfEventAction(const G4Event* evt)
{
  G4int EventNumber= evt->GetEventID();
  G4cout << ">>> Event " << EventNumber << G4endl;
  if(aRun != NULL && !OnlyRootOutput)
  {
    *(aRun->outfile) << "Event " << std::setw(6) << std::setfill(' ') << EventNumber << ":" << std::endl;
  }

  if(ThomsonCollID<0)
  {
    G4cout << "No Hit Collection ID found" << G4endl;
  }
  else
  {
    G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
    ThomsonScreenHitCollection* CHC = 0;

    if(HCE)
      CHC = (ThomsonScreenHitCollection*)(HCE->GetHC(ThomsonCollID));

    if(CHC)
    {
      G4int n_hit = CHC->entries();
      for(G4int iHit = 0; iHit < n_hit; ++iHit)
      {
        ThomsonScreenHit* Hit = (*CHC)[iHit];
        G4ThreeVector WorldPos = Hit->GetWorldPos()/CLHEP::cm;
        G4ThreeVector LocalPos = Hit->GetLocalPos()/CLHEP::cm;

        if(aRun != NULL)
        {
          if(!OnlyRootOutput)
          {
            *(aRun->outfile) << "  Hit ";
            *(aRun->outfile) << std::setw(6) << std::setfill(' ') << iHit << ":";
            *(aRun->outfile) << "  World (x = " << std::setw(7) << std::fixed << std::setprecision(4) << WorldPos.x();
            *(aRun->outfile) << ", y = " << std::setw(7) << std::fixed << std::setprecision(4) << WorldPos.y();
            *(aRun->outfile) << ", z = " << std::setw(7) << std::fixed << std::setprecision(4) << WorldPos.z() << ");";

            *(aRun->outfile) << "  Local (x = " << std::setw(7) << std::fixed << std::setprecision(4) << LocalPos.x();
            *(aRun->outfile) << ", y = " << std::setw(7) << std::fixed << std::setprecision(4) << LocalPos.y();
            *(aRun->outfile) << ", z = " << std::setw(7) << std::fixed << std::setprecision(4) << LocalPos.z() << ")";

            *(aRun->outfile) << std::endl;
          }


          G4double LocalX, LocalY, LocalZ, GlobalX, GlobalY, GlobalZ;

          aRun->outtree->SetBranchAddress("Event", &EventNumber);
          aRun->outtree->SetBranchAddress("LocalX", &LocalX);
          aRun->outtree->SetBranchAddress("LocalY", &LocalY);
          aRun->outtree->SetBranchAddress("LocalZ", &LocalZ);
          aRun->outtree->SetBranchAddress("GlobalX", &GlobalX);
          aRun->outtree->SetBranchAddress("GlobalY", &GlobalY);
          aRun->outtree->SetBranchAddress("GlobalZ", &GlobalZ);

          LocalX  = LocalPos.x();
          LocalY  = LocalPos.y();
          LocalZ  = LocalPos.z();
          GlobalX = WorldPos.x();
          GlobalY = WorldPos.y();
          GlobalZ = WorldPos.z();

          aRun->outtree->Fill();
          aRun->outtree->ResetBranchAddresses();
        }
        else
        {
          G4cout << "Hit " << iHit << G4endl;

          //G4double x, y, z;
          G4cout << "\tWorld - x: " << WorldPos.x() << "; y: " << WorldPos.y() << "; z: " << WorldPos.z() << G4endl;

          G4cout << "\tLocal - x: " << LocalPos.x() << "; y: " << LocalPos.y() << "; z: " << LocalPos.z();
          G4cout << G4endl;
        }
      }
    }
    else
      G4cout << "No Hit Collection found" << G4endl;
  }

  if(ThomsonEyeCollID<0)
  {
    G4cout << "No Eye Hit Collection ID found" << G4endl;
  }
  else
  {
    G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
    ThomsonEyeHitCollection* CHC = 0;

    if(HCE)
      CHC = (ThomsonEyeHitCollection*)(HCE->GetHC(ThomsonEyeCollID));

    if(CHC)
    {
      G4int n_hit = CHC->entries();
      for(G4int iHit = 0; iHit < n_hit; ++iHit)
      {
        ThomsonEyeHit* Hit = (*CHC)[iHit];
        G4ThreeVector WorldPos = Hit->GetWorldPos()/CLHEP::cm;
        G4ThreeVector LocalPos = Hit->GetLocalPos()/CLHEP::cm;
        G4double Energy = Hit->GetEnergy()/CLHEP::eV;
        G4ThreeVector Direction = Hit->GetDirection();
        G4ThreeVector LocalDirection = Hit->GetLocalDirection();

        if(aRun != NULL)
        {
          if(!OnlyRootOutput)
          {
            *(aRun->outfile) << "  Eye Hit ";
            *(aRun->outfile) << std::setw(6) << std::setfill(' ') << iHit << ":";
            *(aRun->outfile) << "  World (x = " << std::setw(7) << std::fixed << std::setprecision(4) << WorldPos.x();
            *(aRun->outfile) << ", y = " << std::setw(7) << std::fixed << std::setprecision(4) << WorldPos.y();
            *(aRun->outfile) << ", z = " << std::setw(7) << std::fixed << std::setprecision(4) << WorldPos.z() << ");";

            *(aRun->outfile) << "  Local (x = " << std::setw(7) << std::fixed << std::setprecision(4) << LocalPos.x();
            *(aRun->outfile) << ", y = " << std::setw(7) << std::fixed << std::setprecision(4) << LocalPos.y();
            *(aRun->outfile) << ", z = " << std::setw(7) << std::fixed << std::setprecision(4) << LocalPos.z() << ");";

            *(aRun->outfile) << "  Energy (" << std::setw(7) << std::fixed << std::setprecision(4) << Energy << " eV );";

            *(aRun->outfile) << "  Direction (x = " << std::setw(7) << std::fixed << std::setprecision(4) << Direction.x();
            *(aRun->outfile) << ", y = " << std::setw(7) << std::fixed << std::setprecision(4) << Direction.y();
            *(aRun->outfile) << ", z = " << std::setw(7) << std::fixed << std::setprecision(4) << Direction.z() << ");";

            *(aRun->outfile) << "  Local Direction (x = " << std::setw(7) << std::fixed << std::setprecision(4) << LocalDirection.x();
            *(aRun->outfile) << ", y = " << std::setw(7) << std::fixed << std::setprecision(4) << LocalDirection.y();
            *(aRun->outfile) << ", z = " << std::setw(7) << std::fixed << std::setprecision(4) << LocalDirection.z() << ");";

            *(aRun->outfile) << std::endl;
          }


          G4double LocalX, LocalY, LocalZ, GlobalX, GlobalY, GlobalZ, DirX, DirY, DirZ, LDirX, LDirY, LDirZ;

          aRun->eyetree->SetBranchAddress("Event", &EventNumber);
          aRun->eyetree->SetBranchAddress("LocalX", &LocalX);
          aRun->eyetree->SetBranchAddress("LocalY", &LocalY);
          aRun->eyetree->SetBranchAddress("LocalZ", &LocalZ);
          aRun->eyetree->SetBranchAddress("GlobalX", &GlobalX);
          aRun->eyetree->SetBranchAddress("GlobalY", &GlobalY);
          aRun->eyetree->SetBranchAddress("GlobalZ", &GlobalZ);
          aRun->eyetree->SetBranchAddress("Energy", &Energy);
          aRun->eyetree->SetBranchAddress("DirectionX", &DirX);
          aRun->eyetree->SetBranchAddress("DirectionY", &DirY);
          aRun->eyetree->SetBranchAddress("DirectionZ", &DirZ);
          aRun->eyetree->SetBranchAddress("LocalDirectionX", &LDirX);
          aRun->eyetree->SetBranchAddress("LocalDirectionY", &LDirY);
          aRun->eyetree->SetBranchAddress("LocalDirectionZ", &LDirZ);

          LocalX  = LocalPos.x();
          LocalY  = LocalPos.y();
          LocalZ  = LocalPos.z();
          GlobalX = WorldPos.x();
          GlobalY = WorldPos.y();
          GlobalZ = WorldPos.z();
          DirX    = Direction.x();
          DirY    = Direction.y();
          DirZ    = Direction.z();
          LDirX   = LocalDirection.x();
          LDirY   = LocalDirection.y();
          LDirZ   = LocalDirection.z();

          aRun->eyetree->Fill();
          aRun->eyetree->ResetBranchAddresses();
        }
        else
        {
          G4cout << "Eye Hit: " << iHit << G4endl;

          //G4double x, y, z;
          G4cout << "\tWorld - x: " << WorldPos.x() << "; y: " << WorldPos.y() << "; z: " << WorldPos.z() << G4endl;

          G4cout << "\tLocal - x: " << LocalPos.x() << "; y: " << LocalPos.y() << "; z: " << LocalPos.z();
          G4cout << G4endl;
        }
      }
    }
    else
      G4cout << "No Eye Hit Collection found" << G4endl;
  }

  return;
}
