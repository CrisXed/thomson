#include "ThomsonPrimaryGeneratorAction.hh"

#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
//#include "G4SIunits.hh"

#include "CLHEP/Random/Random.h"
#include "CLHEP/Random/RanecuEngine.h"
#include "CLHEP/Random/RandFlat.h"
#include "CLHEP/Random/RandGauss.h"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

#define VERBOSE false

extern G4double BeamVoltage;
extern G4double BulbDiameter;
extern G4double RibbonWidth;

ThomsonPrimaryGeneratorAction::ThomsonPrimaryGeneratorAction()
{
  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName = "e-";
  G4ParticleDefinition* particle = particleTable->FindParticle(particleName);
  particleGun->SetParticleDefinition(particle);
}

ThomsonPrimaryGeneratorAction::~ThomsonPrimaryGeneratorAction()
{
  delete particleGun;
}

void ThomsonPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  static CLHEP::RanecuEngine myEngine;
  CLHEP::HepRandomEngine* old_engine = CLHEP::HepRandom::getTheEngine();

  CLHEP::HepRandom::setTheEngine(&myEngine);

  double x, y, z;
  double width = RibbonWidth/CLHEP::cm;
  z = CLHEP::RandFlat::shoot(width/-2,width/2);
  y = CLHEP::RandFlat::shoot(-0.015,0.015);
  x = (BulbDiameter/CLHEP::cm)/-2;

  G4double energy = CLHEP::RandGauss::shoot(BeamVoltage/CLHEP::kilovolt, 0.015)*CLHEP::keV;
  particleGun->SetParticleEnergy(energy);
  particleGun->SetParticlePosition(G4ThreeVector(x*CLHEP::cm, y*CLHEP::cm, z*CLHEP::cm));
  G4ThreeVector v(1.0,0.0,0.0);
  particleGun->SetParticleMomentumDirection(v);
  particleGun->GeneratePrimaryVertex(anEvent);

  if(VERBOSE)
  {
    G4cout << "Generated an electron from a distribution with mean: " << BeamVoltage/CLHEP::kilovolt << " keV and std_dev: 15 eV" << std::endl;
    G4cout << "The electron had an energy of: " << energy/CLHEP::keV << " keV" << std::endl;
    G4cout << "The starting position was: x=-11.5cm, y=" << y << "cm, z=" << z << "cm" << std::endl;
  }

  CLHEP::HepRandom::setTheEngine(old_engine);
}
