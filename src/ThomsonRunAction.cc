#include "ThomsonRunAction.hh"

#include "G4Run.hh"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

#include <sstream>
#include <iomanip>
#include <fstream>

#include "TROOT.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TTree.h"
#include "TVectorD.h"

extern G4double PlateVoltage;
extern G4double CoilCurrent;
extern G4double BeamVoltage;
extern bool OnlyRootOutput;
extern G4double ScreenWidth;
extern G4double ScreenHeight;
extern G4double ScreenAngle;
extern bool RealisticFields;

std::string itoa_len(int number, int len)
{
  std::stringstream ss; //create a stringstream
  ss << std::setw(len) << std::setfill('0') << number;    //add number to the stream
  return ss.str(); //return a string with the contents of the stream
}

ThomsonRunAction::ThomsonRunAction()
{
  outfile_root = new TFile("Output.root", "RECREATE");

  CurrentDir = gDirectory;
}

ThomsonRunAction::~ThomsonRunAction()
{
  delete outfile_root;
}

void ThomsonRunAction::BeginOfRunAction(const G4Run* aRun)
{
  runID = aRun->GetRunID();

  G4cout << "### Run " << itoa_len(runID,3) << G4endl;

  filename = "Output_Run";
  filename += itoa_len(runID,3);
  filename += ".dat";

  if(!OnlyRootOutput)
    outfile = new std::ofstream(filename.c_str(), std::ios::out);

  std::string dirname = "Run_";
  dirname += itoa_len(runID,3);
  RunDir = outfile_root->mkdir(dirname.c_str());
  RunDir->cd();

  outtree = new TTree("Hits", "Hits on the screen");

  G4int evt;
  G4double val;
  outtree->Branch("Event", &evt);
  outtree->Branch("LocalX", &val);
  outtree->Branch("LocalY", &val);
  outtree->Branch("LocalZ", &val);
  outtree->Branch("GlobalX", &val);
  outtree->Branch("GlobalY", &val);
  outtree->Branch("GlobalZ", &val);

  outtree->ResetBranchAddresses();


  eyetree = new TTree("EyeHits", "Hits on the eye");

  eyetree->Branch("Event", &evt);
  eyetree->Branch("LocalX", &val);
  eyetree->Branch("LocalY", &val);
  eyetree->Branch("LocalZ", &val);
  eyetree->Branch("GlobalX", &val);
  eyetree->Branch("GlobalY", &val);
  eyetree->Branch("GlobalZ", &val);
  eyetree->Branch("Energy", &val);
  eyetree->Branch("DirectionX", &val);
  eyetree->Branch("DirectionY", &val);
  eyetree->Branch("DirectionZ", &val);
  eyetree->Branch("LocalDirectionX", &val);
  eyetree->Branch("LocalDirectionY", &val);
  eyetree->Branch("LocalDirectionZ", &val);

  eyetree->ResetBranchAddresses();
}

void ThomsonRunAction::EndOfRunAction(const G4Run* aRun)
{
  if(!OnlyRootOutput)
  {
    outfile->close();
    delete outfile;
  }

  outtree->Write();
  eyetree->Write();

  TVectorD StatusVec(7);
  StatusVec[0] = BeamVoltage/CLHEP::kilovolt;
  StatusVec[1] = PlateVoltage/CLHEP::kilovolt;
  StatusVec[2] = CoilCurrent/CLHEP::ampere;
  StatusVec[3] = ScreenWidth/CLHEP::cm;
  StatusVec[4] = ScreenHeight/CLHEP::cm;
  StatusVec[5] = ScreenAngle/CLHEP::deg;
  if(RealisticFields)
    StatusVec[6] =  1.0;
  else
    StatusVec[6] = -1.0;
  StatusVec.Write("StatusVec");

  CurrentDir->cd();
}
