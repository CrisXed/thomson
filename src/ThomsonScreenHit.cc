#include "ThomsonScreenHit.hh"
#include "G4UnitsTable.hh"
#include <sstream>

G4Allocator<ThomsonScreenHit> ThomsonScreenHitAllocator;

ThomsonScreenHit::ThomsonScreenHit()
{
}

ThomsonScreenHit::~ThomsonScreenHit()
{
}

void ThomsonScreenHit::Print()
{
  //G4cout << " ThomsonScreenHit:" << "Energy deposit: " << G4BestUnit(HitEdep,"Energy") << G4endl;
}
