#include "ThomsonPhysicsList.hh"

#include "G4EmLivermorePhysics.hh"
#include "G4ProcessManager.hh"

//Bosons
#include "G4Gamma.hh"
#include "G4OpticalPhoton.hh"
//Leptons
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4NeutrinoE.hh"
#include "G4AntiNeutrinoE.hh"
//Barions
#include "G4Proton.hh"
#include "G4Neutron.hh"
#include "G4AntiProton.hh"
#include "G4AntiNeutron.hh"
//Mesons
#include "G4MesonConstructor.hh"
//Ions
#include "G4Alpha.hh"
#include "G4GenericIon.hh"

#include "G4Decay.hh"

#include "G4Scintillation.hh"

#include "G4OpAbsorption.hh"
//#include "G4OpRayleigh.hh"
//#include "G4OpMieHG.hh"
#include "G4OpBoundaryProcess.hh"

#include "G4EmProcessOptions.hh"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

ThomsonPhysicsList::ThomsonPhysicsList(double scintillationYield)
  :G4VUserPhysicsList()
  ,scintillationYield_(scintillationYield)
{
  defaultCutValue = 0.1*CLHEP::mm;
  fCutForGamma = 0.01*CLHEP::mm;
  SetVerboseLevel(1);

  EMPhysicsList = new G4EmLivermorePhysics();
}

ThomsonPhysicsList::~ThomsonPhysicsList()
{
  delete EMPhysicsList;
}

void ThomsonPhysicsList::ConstructParticle()
{
  G4Gamma::GammaDefinition();
  G4OpticalPhoton::OpticalPhotonDefinition();

  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();
  G4NeutrinoE::NeutrinoEDefinition();
  G4AntiNeutrinoE::AntiNeutrinoEDefinition();

  G4MesonConstructor mConstructor;
  mConstructor.ConstructParticle();

  G4Proton::ProtonDefinition();
  G4Neutron::NeutronDefinition();
  G4AntiProton::AntiProtonDefinition();
  G4AntiNeutron::AntiNeutronDefinition();

  G4Alpha::AlphaDefinition();
  G4GenericIon::GenericIonDefinition();
}

void ThomsonPhysicsList::ConstructProcess()
{
  AddTransportation();
  ConstructGeneral();
  ConstructEM();

  // Em options
  //
  G4EmProcessOptions emOptions;

  //physics tables
  //
  //emOptions.SetMinEnergy(10*eV);        //default 100 eV
  //emOptions.SetMaxEnergy(10*TeV);        //default 100 TeV
  //emOptions.SetDEDXBinning(12*20);        //default=12*7
  //emOptions.SetLambdaBinning(12*20);        //default=12*7
}

void ThomsonPhysicsList::ConstructEM()
{
  EMPhysicsList->ConstructProcess();

  theScintillationProcess = new G4Scintillation("Scintillation");
  theAbsorptionProcess         = new G4OpAbsorption();
  //theRayleighScatteringProcess = new G4OpRayleigh();
  //theMieHGScatteringProcess    = new G4OpMieHG();
  theBoundaryProcess           = new G4OpBoundaryProcess();

  // Edit here to suppress scintillation----------------------------------------
  theScintillationProcess->SetScintillationYieldFactor(scintillationYield_);
  //theScintillationProcess->SetTrackSecondariesFirst(true);

  theParticleIterator->reset();
  while( (*theParticleIterator)() )
  {
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();

    if (theScintillationProcess->IsApplicable(*particle))
    {
      pmanager->AddProcess(theScintillationProcess);
      pmanager->SetProcessOrderingToLast(theScintillationProcess, idxAtRest);
      pmanager->SetProcessOrderingToLast(theScintillationProcess, idxPostStep);
    }
    if (particleName == "opticalphoton")
    {
      G4cout << " AddDiscreteProcess to OpticalPhoton " << G4endl;
      pmanager->AddDiscreteProcess(theAbsorptionProcess);
      //pmanager->AddDiscreteProcess(theRayleighScatteringProcess);
      //pmanager->AddDiscreteProcess(theMieHGScatteringProcess);
      pmanager->AddDiscreteProcess(theBoundaryProcess);
    }
  }
}

void ThomsonPhysicsList::ConstructGeneral()
{
  G4Decay* theDecayProcess = new G4Decay();
  theParticleIterator->reset();
  while((*theParticleIterator)())
  {
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    if(theDecayProcess->IsApplicable(*particle))
    {
      pmanager->AddProcess(theDecayProcess);

      pmanager->SetProcessOrdering(theDecayProcess, idxPostStep);
      pmanager->SetProcessOrdering(theDecayProcess, idxAtRest);
    }
  }
}

void ThomsonPhysicsList::SetCuts()
{
  G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(1*CLHEP::eV,1*CLHEP::GeV);

  SetCutValue(fCutForGamma, "gamma");
  SetCutValue(defaultCutValue, "e-");
  SetCutValue(defaultCutValue, "e+");

  DumpCutValuesTable();
}
