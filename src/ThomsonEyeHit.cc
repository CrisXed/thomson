#include "ThomsonEyeHit.hh"
#include "G4UnitsTable.hh"
#include <sstream>

G4Allocator<ThomsonEyeHit> ThomsonEyeHitAllocator;

ThomsonEyeHit::ThomsonEyeHit()
{
}

ThomsonEyeHit::~ThomsonEyeHit()
{
}

void ThomsonEyeHit::Print()
{
  //G4cout << " ThomsonEyeHit:" << "Energy deposit: " << G4BestUnit(HitEdep,"Energy") << G4endl;
}
