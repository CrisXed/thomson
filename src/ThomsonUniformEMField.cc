#include "ThomsonUniformEMField.hh"
#include "G4ThreeVector.hh"


ThomsonUniformEMField::ThomsonUniformEMField()
    :_Bfield(0.0, 0.0, 0.0),
     _Efield(0.0, 0.0, 0.0)
{}

ThomsonUniformEMField::ThomsonUniformEMField(G4ThreeVector& B, G4ThreeVector& E)
    :_Bfield(B),
     _Efield(E)
{}

ThomsonUniformEMField::ThomsonUniformEMField(G4double Bx, G4double By, G4double Bz, G4double Ex, G4double Ey, G4double Ez)
    :_Bfield(Bx, By, Bz),
     _Efield(Ex, Ey, Ez)
{}

ThomsonUniformEMField::~ThomsonUniformEMField() {}

void ThomsonUniformEMField::GetFieldValue(const G4double Point[4], G4double *Bfield) const
{
  Bfield[0] = _Bfield.x();
  Bfield[1] = _Bfield.y();
  Bfield[2] = _Bfield.z();
  Bfield[3] = _Efield.x();
  Bfield[4] = _Efield.y();
  Bfield[5] = _Efield.z();
}

G4bool ThomsonUniformEMField::DoesFieldChangeEnergy() const
{
  if(_Efield.x() == 0.0 && _Efield.y() == 0.0 && _Efield.z() == 0.0)
    return false;
  return true;
}

void ThomsonUniformEMField::SetFields(G4double Bx, G4double By, G4double Bz, G4double Ex, G4double Ey, G4double Ez)
{
  _Bfield.set(Bx, By, Bz);
  _Efield.set(Ex, Ey, Ez);
  return;
}

void ThomsonUniformEMField::SetEField(G4double Ex, G4double Ey, G4double Ez)
{
  _Efield.set(Ex, Ey, Ez);
  return;
}

void ThomsonUniformEMField::SetBField(G4double Bx, G4double By, G4double Bz)
{
  _Bfield.set(Bx, By, Bz);
  return;
}
